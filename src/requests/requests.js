export const updateUserActivities = (activities) => {
    return fetch(`http://localhost:8000/activities`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({activities})
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.status;
        })
}

export const getUsername = () => {
    return fetch("http://localhost:8000/profile", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(resp => resp.json())
}

export const isMoodFilled = () => {
    return fetch("http://localhost:8000/mood", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(resp => resp.json())
}

export const userActivitiesFetch = (activities) => {
    const token = localStorage.getItem("token");
    const selectedActivities = activities;

    selectedActivities.forEach(item => {
        item.options = JSON.parse(localStorage.getItem(item.type));
    })

    const userData = {
        mood: localStorage.getItem("mood"),
        activities: selectedActivities
    }

    return fetch(`http://localhost:8000/mood`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({userData})
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.status;
        })
}

export const getStatistics = () => {
    return fetch(`http://localhost:8000/statistics`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(res => res.json())
}

const getRandom = (max) => {
    return Math.floor(Math.random() * max);
}

export const getQuote = () => {
    return fetch("https://type.fit/api/quotes")
        .then(response => response.json())
        .then(data => data[getRandom(data.length)]);
};

// export const getImage = (name) => {
//     const key = '21554890-54e7c572fe76abd3b9b75fc15';
//     if (name) {
//         return fetch(`https://pixabay.com/api/?key=${key}&q=${name.replace(' ', '+')}`)
//             .then(response => response.json())
//             .then(res => {
//                 console.log(res)
//                 if (res.hits.length > 0) {
//                     console.log(res.hits[getRandom(res.hits.length)].largeImageURL)
//                     return res.hits[getRandom(res.hits.length)].largeImageURL;
//                 }
//                 return null;
//             })
//     } else {
//         return null;
//     }
// }
