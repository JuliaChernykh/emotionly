import React, {useEffect, useState} from 'react';
import {NavLink, Redirect} from "react-router-dom";
import {connect} from "react-redux";

import styles from "../components/registration/RegistrationForm.module.css";
import styles1 from "./DaiyActivityPage.module.css";
import activityBoard from "../images/activityBoard.png";

import Button from "../components/button/Button";
import Layout from "../components/layout/Layout";
import ActivityBoard from "../components/activityBoard/ActivityBoard";
import NavBar from "../components/navbar/NavBar";

import {getUsername} from "../requests/requests";
import {userActivitiesFetch} from "../requests/requests";

const DailyActivityPage = ({userActivities}) => {
    const [username, setUsername] = useState('');
    const [successSaving, setSuccessSaving] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        getUsername()
            .then(response => {
                setUsername(response.user.username);
            })
    }, [])

    const handleClick = (event) => {
        event.preventDefault();
        let flag = false;
        userActivities.forEach(activity => {
            if (localStorage.getItem(activity.type) !== null) {
                flag = true;
            }
        })
        if (flag) {
            userActivitiesFetch(userActivities)
                .then(status => {
                    setSuccessSaving(true);
                    userActivities.forEach(activity => {
                        localStorage.removeItem(activity.type)
                    });
                    localStorage.removeItem("mood");
                })
                .catch(errorStatus => {
                    setError(errorStatus);
                    setSuccessSaving(true);
                })
        } else {
            setSuccessSaving(false);
            setError('Choose your activities to continue')
        }
    }
    if (successSaving) {
        return <Redirect to={`/${username}/saved`}></Redirect>
    }
    return (
        <>
            <NavBar/>
            <div className={styles1.pageContent}>
                <h1>What have you been up to?</h1>
            </div>
            <div className={styles1.container}>
                <img className={styles1.boardLayout} src={activityBoard} alt="layout" />
            </div>
            <ActivityBoard/>
            <div className={styles.buttonsRow}>
                <NavLink to={`/`}>
                    <Button id={0}>Back</Button>
                </NavLink>
                <Button id={0} onClick={handleClick}>Save</Button>
            </div>
            {error ? <h2 className={styles.errorLabel1}>{error.toString()}</h2> : null}
            <Layout pageName="DailyActivityPage"/>
        </>
    );
};

const mapStateToProps = state => ({ userActivities: state.activities });

export default connect(mapStateToProps, null)(DailyActivityPage);
