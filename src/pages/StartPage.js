import React, {useEffect, useState} from 'react';
import { NavLink } from "react-router-dom";

import styles from "../App.module.css";
import StartPageCenter from '../images/startPageCenter.png'

import Button from "../components/button/Button";
import Layout from "../components/layout/Layout";
import Loader from "../components/loader/Loader";

import {Redirect} from "react-router";
import {getUsername} from "../requests/requests";

const StartPage = () => {
    const [username, setUsername] = useState('');
    const [isAuthorized, setAuthorized] = useState(false);
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const token = localStorage.getItem("token");
        if (token) {
            getUsername()
                .then(response => {
                    setUsername(response.user.username);
                    setAuthorized(true)
                    setLoaded(true)
                });
        } else {
            setLoaded(true);
        }
    },[])

    if (loaded) {
        if (isAuthorized) {
            return <Redirect to={`/${username}`}/>;
        }
        return (
            <>
                <div className={styles.pageContent}>
                    <img className={styles.centerImage} src={StartPageCenter} alt="layout"/>
                    <h2>your</h2>
                    <h1>emotional diary</h1>
                    <h2>to understand yourself better</h2>
                </div>
                <div className={styles.buttonsRow}>
                    <NavLink
                        to={`/login`}
                    >
                        <Button id={0}>Login</Button>
                    </NavLink>
                    <NavLink
                        to={`/register`}
                    >
                        <Button id={0}>Register</Button>
                    </NavLink>
                </div>
                <Layout pageName="StartPage"/>
            </>
        );
    } else {
        return (
            <>
                <Loader/>
                <Layout pageName="StartPage"/>
            </>
        )
    }
};

export default StartPage;


