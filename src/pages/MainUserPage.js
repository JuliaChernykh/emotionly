import React, {useEffect, useState} from 'react';
import { NavLink } from "react-router-dom";

import styles from "../components/registration/RegistrationForm.module.css";
import styles1 from "../App.module.css";
import quoteBackground from "../images/quoteBackground.png"

import Button from "../components/button/Button";
import EmotionsList from "../components/emotionslist/EmotionsList";
import Layout from "../components/layout/Layout";
import NavBar from "../components/navbar/NavBar";

import {getQuote, getUsername} from "../requests/requests";
import {isMoodFilled} from "../requests/requests";
import Loader from "../components/loader/Loader";
import {Redirect} from "react-router";

const MainUserPage = () => {
    const [moodFilled, setMoodFilled] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [username, setUsername] = useState('');
    const [moodChosen, setMoodChosen] = useState(false);
    const [error, setError] = useState('');
    const [quote, setQuote] = useState('');

    useEffect(() => {
        setTimeout(function() {
            isMoodFilled()
                .then(flag => {
                    if (flag) {
                        getQuote()
                            .then(quote => {
                                setQuote(quote);
                                setMoodFilled(true);
                                setLoaded(true);
                            });
                    } else {
                        setLoaded(true);
                    }
                })
        }, 400);

        getUsername()
            .then(response => {
                setUsername(response.user.username);
            })
    },[])


    const handleClick = (event) => {
        event.preventDefault();
        setMoodFilled(false);
    }

    const handleRedirectionClick = (event) => {
        event.preventDefault();
        if (localStorage.getItem("mood")) {
            setMoodChosen(true);
        } else {
            setMoodChosen(false);
            setError('Choose your mood to continue')
        }
    }


    if (!loaded) {
        return <Loader/>
    }
    else if (moodFilled) {
        return (
            <>
                <NavBar/>
                <div className={styles1.pageContent}>
                    <h2>Today you've already filled your mood.</h2>
                    <h1>Come back tomorrow!</h1>
                </div>
                <div className={styles.buttonsRow}>
                    <NavLink
                        to={`/${username}/statistics`}
                    >
                        <Button id={0}>See statistics</Button>
                    </NavLink>
                    <Button id={0} onClick={handleClick}>Edit mood</Button>
                </div>
                {quote ?
                    <div className={styles.quote}>
                        <img className={styles.background} src={quoteBackground} alt="background"/>
                        <h2 className={styles.quoteText}>"{quote.text}"</h2>
                        <h2 className={styles.quoteAuthor}>{quote.author}</h2>
                    </div>
                    :
                    null
                }
                <Layout pageName="MainUserPage"/>
            </>
        );
    }
    else if (!moodChosen) {
        return (
            <>
                <NavBar/>
                <div className={styles1.pageContent}>
                    <h2>hello, {username}</h2>
                    <h1>How are you today?</h1>
                </div>
                <EmotionsList/>
                <div className={styles.buttonAlone}>
                    <Button id={0} onClick={handleRedirectionClick}>Continue</Button>
                </div>
                {error ? <h2 className={styles.errorLabel1}>{error.toString()}</h2> : null}
                <Layout pageName="MainUserPage"/>
            </>
        );
    }
    else {
        return <Redirect to={`/${username}/activity`}/>;
    }
};

export default MainUserPage;
