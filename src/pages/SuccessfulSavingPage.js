import React, {useEffect, useState} from 'react';
import {Redirect} from "react-router";
import {NavLink} from "react-router-dom";

import styles from "../App.module.css";

import Layout from "../components/layout/Layout";
import NavBar from "../components/navbar/NavBar";
import Button from "../components/button/Button";

import {getUsername} from "../requests/requests";
import {isMoodFilled} from "../requests/requests";
import Loader from "../components/loader/Loader";


const SuccessfulSavingPage = () => {
    const [moodFilled, setMoodFilled] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [username, setUsername] = useState('');

    useEffect(() => {
        setTimeout(function() {
            isMoodFilled()
                .then(flag => {
                    if (flag) {
                        setMoodFilled(true);
                        setLoaded(true);
                    }
                })
        }, 400);

        getUsername()
            .then(response => {
                setUsername(response.user.username);
            })
    },[])

    if (loaded) {
        if (moodFilled) {
            return (
                <>
                    <NavBar/>
                    <div className={styles.pageContent}>
                        <h1>Your mood has been successfully saved!</h1>
                    </div>
                    <div className={styles.buttonsRow}>
                        <NavLink
                            to={`/${username}/statistics`}
                        >
                            <Button id={0}>See statistics</Button>
                        </NavLink>
                        <NavLink
                            to={`/main`}
                        >
                            <Button id={0}>Back to main page</Button>
                        </NavLink>
                    </div>
                    <Layout pageName="SuccessfulSavingPage"/>
                </>
            );
        }
        else {
            return <Redirect to="/" />
        }
    } else {
        return <Loader/>;
    }

};

export default SuccessfulSavingPage;
