import React from 'react';
import {NavLink} from "react-router-dom";

import styles from "../components/registration/RegistrationForm.module.css";
import styles1 from "../App.module.css";

import Button from "../components/button/Button";
import Layout from "../components/layout/Layout";


const NotFoundPage = () => {
    return (
        <>
            <div className={styles1.pageContent}>
                <h1>Oops.. This page doesn't exist</h1>
            </div>
            <div className={styles.buttonAlone}>
                <NavLink
                    to={`/`}
                >
                    <Button id={0}>Return</Button>
                </NavLink>
            </div>
            <Layout pageName="NotFountPage"/>
        </>
    );
};

export default NotFoundPage;
