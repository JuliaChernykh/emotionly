import React, {useState, useEffect} from 'react';
import Layout from "../components/layout/Layout";
import NavBar from "../components/navbar/NavBar";
import EmotionsFrequency from "../components/emotionsFrequencyStatistics/EmotionsFrequency";
import ActivitiesTop from "../components/activitiesTop/ActivitiesTop";

import {connect} from "react-redux";
import {loadStatistics} from "../redux/statistics/actions";

import styles from './StatisticsPage.module.css'
import StatisticsCorrelation from "../components/statisticsCorrelation/StatisticsCorrelation";
import Loader from "../components/loader/Loader";
import Button from "../components/button/Button";
import {NavLink} from "react-router-dom";

const StatisticsPage = ({statistics, loadStatistics}) => {
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        loadStatistics()
            .then(data => {
                setLoaded(true);
            })
    }, [loadStatistics]);


    if (loaded) {
        return (
            <>
                <NavBar/>
                {statistics.total > 0 ?
                    <>
                        <div className={styles.pageHeader}>
                            <h1>Your statistics here</h1>
                        </div>
                        <EmotionsFrequency total={statistics.total} data={statistics.mood} />
                        {statistics.total > 1 ? <ActivitiesTop total={statistics.total} data={statistics.activities} /> : null}
                        {statistics.total > 1 ? <StatisticsCorrelation data={statistics.correlation}/> : null}
                    </> :
                    <h1 className={styles.label}>Fill in your day for the first time and see your statistics here!</h1>
                }
                <div className={styles.buttonAlone}>
                    <NavLink to={`/`}>
                        <Button id={0}>Back to main</Button>
                    </NavLink>
                </div>
                <Layout pageName="StatisticsPage"/>
            </>
        );
    } else {
        return <Loader/>
    }
};

const mapStateToProps = state => ({ statistics: state.statistics });

const mapDispatchToProps = dispatch => {
    return {
        loadStatistics: () => dispatch(loadStatistics()),
        dispatch,
    }
}

export {StatisticsPage};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StatisticsPage);

