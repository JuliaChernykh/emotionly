export const sendActivities = (selectedActivities) => {
    return fetch("http://localhost:8000/profile", {
        method: "P0ST",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({selectedActivities})
    })
        .then((response) => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(resp => resp.json())
        .then(data => {
            console.log(data);
        })
}

export const getActivities = () => {
    const token = localStorage.getItem("token");
    return fetch(`http://localhost:8000/activities`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(resp => resp.json())
}
