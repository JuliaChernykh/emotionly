import React, {useState, useEffect} from 'react';
import {NavLink, Redirect} from "react-router-dom";

import {connect} from 'react-redux';

import styles from './RegistrationForm.module.css';

import Button from "../button/Button";
import Layout from "../layout/Layout";
import {userPostFetch} from "../../redux/authorization/actions";
import registrationCenter from "../../images/registrationCenter.png";
import classNames from "classnames/bind";

const RegistrationForm = ({userPostFetch}) => {
    const cx = classNames.bind(styles);
    const [didMount, setDidMount] = useState(false);

    const [inputValues, setInputValues] = useState({email: '', username: '', password: ''});
    const [registrationStatus, setStatus] = useState(false);
    const [error, setError] = useState('');
    const [inputError, setInputError] = useState('');
    const [userName, setUsername] = useState('')


    useEffect(() => {
        setError('');
        setDidMount(true);
        return () => setDidMount(false);
    }, [])

    if(!didMount) {
        return null;
    }


    const handleSubmit = (event) => {
        event.preventDefault();
        if (inputValues.email && inputValues.username && inputValues.password) {
            userPostFetch(inputValues)
                .then((username) => {
                    setUsername(username);
                    setStatus(true);
                    setError('');
                })
                .catch(err => {
                    err = JSON.parse(err)
                    setStatus(false);
                    setError(err);
                });
        } else {
            setInputError('Error: Fill in all fields')
        }
    }
    const handleInputChange = (event) => {
        const value = event.target.value;
        setInputValues({
            ...inputValues,
            [event.target.name]: value
        });
    };

    if (registrationStatus) {
        return <Redirect to={`/${userName}`}></Redirect>;
    }
    return (
        <>
        <form onSubmit={handleSubmit}>
            <img className={styles.centerImage} src={registrationCenter} alt="centerImage" />
            <h1>Register</h1>
            <div className={styles.inputsBlock}>
                <input className={cx({errorInput: error.username || inputError})}
                       type="text" value={inputValues.username}
                       name="username"
                       placeholder="Nickname"
                       onChange={handleInputChange} />
                {error.username ? <h2 className={styles.errorLabel}>{error.username.toString()}</h2> : null}
                <input className={cx({errorInput: error.email || inputError})}
                       type="text"
                        value={inputValues.email}
                        name="email"
                        placeholder="Email"
                        onChange={handleInputChange} />
                {error.email ? <h2 className={styles.errorLabel}>{error.email.toString()}</h2> : null}
                <input className={cx({errorInput: error.password || inputError})}
                       type="password"
                       value={inputValues.password}
                       name="password"
                       placeholder="Password"
                       onChange={handleInputChange} />
                {error.password ? <h2 className={styles.errorLabel}>{error.password.toString()}</h2> : null}
                {inputError ? <h2 className={styles.errorLabel}>{inputError}</h2> : null}
            </div>
            <div className={styles.buttonAlone}>
                <Button id={0}>Create new account</Button>
            </div>
            <div className={styles.buttonsRow}>
                <NavLink to={`/`}>
                    <Button id={0}>Back</Button>
                </NavLink>
                <NavLink to={`/login`}>
                    <Button id={0}>Login</Button>
                </NavLink>
            </div>
        </form>
            <Layout pageName="RegistrationPage"></Layout>
        </>
    );
};

const mapDispatchToProps = dispatch => ({
    userPostFetch: (userInfo) => dispatch(userPostFetch(userInfo)),
    dispatch,
})
export default connect(null, mapDispatchToProps)(RegistrationForm);

// export default RegistrationForm;
