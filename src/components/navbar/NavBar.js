import React, {useState} from 'react';
import * as FaIcons from 'react-icons/fa';
import * as CgIcons from 'react-icons/cg'
import { IconContext } from 'react-icons';
import classNames from 'classnames/bind';
import { NavLink } from 'react-router-dom';

import { SideBarData } from "./SideBarData";

import {logoutUser} from "../../redux/authorization/actions";

import styles from './NavBar.module.css';
import {Redirect} from "react-router";
import {getUsername} from "../../requests/requests";

const NavBar = () => {
    const cx = classNames.bind(styles);
    const [sidebar, setSidebar] = useState(false);
    const [logoutState, setLogoutState] = useState(false)
    const [statisticState, setStatisticState] = useState(false)
    const [mainPageState, setMainPageState] = useState(false);
    const [username, setUsername] = useState('');

    const changeSidebarState = () => setSidebar(!sidebar)

    const handleClick = (event, index) => {
        event.preventDefault();
        if (index === 2) {
            localStorage.removeItem("token");
            logoutUser();
            setLogoutState(true);
        } else if (index === 1) {
            getUsername()
                .then(response => {
                    setUsername(response.user.username);
                    setStatisticState(true);
                })
        } else {
            setMainPageState(true);
        }
    }

    if (logoutState) {
        localStorage.clear();
        return <Redirect to="/"></Redirect>;
    }
    if (statisticState) {
        if (window.location.pathname !== `/${username}/statistics`) {
            return <Redirect to={`/${username}/statistics`}></Redirect>
        }
    }
    if (mainPageState) {
        return <Redirect to="/"></Redirect>
    }
    return (
        <IconContext.Provider value={{color: '#000000', size: 25}}>
            <div className={styles.navbar}>
                <FaIcons.FaBars onClick={changeSidebarState} />
            </div>

                <nav className={cx({
                    navMenu: true,
                    active: sidebar
                })}>
                    <ul>
                        <li onClick={changeSidebarState}>
                            <CgIcons.CgClose/>
                        </li>
                        {SideBarData.map((item, index) => {
                            return (
                                <li key={index}>
                                    <NavLink to={item.path}>
                                        <span
                                            key={index}
                                            onClick={(event) => handleClick(event, index)}
                                        >
                                            {item.title}
                                        </span>
                                    </NavLink>
                                </li>
                            );
                        })}
                    </ul>
                </nav>
        </IconContext.Provider>
    );
};



export default NavBar;


