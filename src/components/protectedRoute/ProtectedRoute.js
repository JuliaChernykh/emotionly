import React, {useEffect, useState} from "react";
import {Route, Redirect} from "react-router-dom";
import {getUsername} from "../../requests/requests";

const ProtectedRoute = ({component: Component, ...rest}) => {
   const token = localStorage.getItem("token");
   const [username, setUsername] = useState('');
   const [loaded, setLoaded] = useState(false);

    const getPathname = () => {
        const index = window.location.pathname.slice(1).indexOf('/');
        let pathname;
        if (index > -1) {
            pathname = window.location.pathname.slice(1, index+1);
        } else {
            pathname = window.location.pathname.slice(1);
        }
        return pathname;
    }

   useEffect(() => {
       getUsername()
           .then(response => {
               setUsername(response.user.username);
               setLoaded(true);
           })
   }, [])

    if (loaded) {
        return (
            <Route
                {...rest}
                render={props => {

                    if (token && (getPathname() === username)) {
                        return <Component {...props} />
                    }
                    return <Redirect to="/"/>
                }}
            />
        );
    } else {
        return (
            <div>Loading...</div>
        );
    }

}

export default ProtectedRoute;
