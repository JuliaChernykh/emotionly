import React, {useEffect, useState} from 'react';

import Button from "../button/Button";

import * as CgIcons from 'react-icons/cg'
import { IconContext } from 'react-icons';

import classNames from 'classnames/bind';
import styles from './ActivityBoard.module.css';

import ActivityBoardItem from "../activityBoardItem/ActivityBoardItem";
import {connect} from "react-redux";

import {updateUserActivities} from "../../requests/requests";
import {loadActivities} from "../../redux/dailyActivities/actions";

const ActivityBoard = ({userActivities, loadActivities}) => {
    const cx = classNames.bind(styles);

    const [editingMode, setEditingMode] = useState(false);
    const [inputMode, setInputMode] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [rerender, setRerender] = useState(false);

    const [activities, setActivities] = useState([]);
    useEffect(() => {
        loadActivities()
            .then(data => {
                setActivities(data);

            })
    }, [loadActivities])

    const handleEditButtonClick = () => {
        if (editingMode) {
            if (inputValue) {
                userActivities.push({
                    id: userActivities.length + 1,
                    type: inputValue,
                    options: []
                });
            }
            setInputValue('');
            setInputMode(false);
            updateUserActivities(userActivities);
        }
        setEditingMode(!editingMode);
    }

    const handleAddButtonClick = () => {
        setInputMode(!inputMode);
    }

    const handleKeyPress = (event) => {
        if(event.key === 'Enter') {
            if (inputValue) {
                userActivities.push({
                    id: userActivities.length + 1,
                    type: inputValue,
                    options: []
                });
            }
            setInputMode(!inputMode);
            setInputValue('');
        }
    }

    const handleInputChange = (event) => {
        setInputValue(event.target.value);
    };

    const handleDeletingClick = (event, index) => {
        event.preventDefault();
        if (editingMode) {
            userActivities.splice(index, 1);
            setRerender(!rerender);
        }
    }

    return (
        <IconContext.Provider value={{color: '#000000', size: 17}}>
            <div className={styles.container}>
                {activities.map((item, index) => (
                    <div key={item.id} className={styles.boardContainer}>

                        <ActivityBoardItem key={item.id} type={item.type} options={item.options}>
                        </ActivityBoardItem>
                        {editingMode ?
                            <button
                                className={cx({editingMode: editingMode, deletingButton: true})}
                                onClick={(event) => handleDeletingClick(event, index)}
                            >
                                <i
                                    key={index}
                                    className={cx({editingMode: editingMode})}
                                >
                                    <CgIcons.CgClose/>
                                </i>
                            </button> :
                            null
                        }

                    </div>
                ))}
            </div>
            <div className={cx({editingBoardButton: true, editingModeFlex: editingMode})}>
                {editingMode ?
                    (<div className={cx({disButton: true, editingMode: editingMode, inputMode: inputMode})}>
                        {inputMode ? null : (<Button id={1} onClick={handleAddButtonClick}>Add new</Button>)}
                        {inputMode ?
                            <input type="text"
                                   value={inputValue}
                                   onChange={handleInputChange}
                                   onKeyPress={handleKeyPress}
                                   maxLength="6"
                            >
                            </input> :
                            null
                        }
                    </div>) :
                    null
                }
                <Button id={1} onClick={handleEditButtonClick}>{editingMode ? 'Save' : 'Edit/New'}</Button>
            </div>
        </IconContext.Provider>
    );
};

const mapStateToProps = state => ({ userActivities: state.activities });

const mapDispatchToProps = dispatch => {
    return {
        loadActivities: () => dispatch(loadActivities()),
        dispatch,
    }
}

export {ActivityBoard};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivityBoard);

