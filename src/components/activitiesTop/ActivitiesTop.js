import React from "react";

import styles from './ActivitiesTop.module.css';
import statisticsItem from '../../images/statisticsItem.png'

const ActivitiesTop = ({total, data}) => {
    let top = new Map();
    for (const key in data) {
        top.set(key, data[key]);
    }
    top = new Map([...top.entries()].sort((a, b) => b[1] - a[1]));
    const activities = [...top.keys()];


    return (
        <>
            <div className={styles.top5Label}>
                <h2>Your personal top-5</h2>
            </div>
            <div className={styles.board}>
                <img className={styles.statisticsItemLayout} src={statisticsItem} alt="layout" />
                <div className={styles.container}>
                    {activities.map((activity, index) =>
                        <div key={index} className={styles.individualStatisticsContainer}>
                            <h2 className={styles.activityItem}>{activity}</h2>
                            {total !== 0 ?
                                <h2 key={Date.now() + 1} className={styles.percents}>
                                    {top.get(activities[index]) ?
                                        Math.floor(((top.get(activities[index])/total)*100)).toString()+'%' :
                                        '0%'
                                    }
                                </h2> :
                                null
                            }
                        </div>
                    )}
                </div>
            </div>
        </>
    );
};

export default ActivitiesTop;
