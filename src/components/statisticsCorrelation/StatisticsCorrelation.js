import React from "react";

import styles from './StatisticsCorrelation.module.css'

import emotion1 from "../../images/emotion1.png";
import emotion2 from "../../images/emotion2.png";
import emotion3 from "../../images/emotion3.png";
import emotion4 from "../../images/emotion4.png";
import emotion5 from "../../images/emotion5.png";
import statisticsItem from "../../images/statisticsItem.png";

const StatisticsCorrelation = ({data}) => {
    const stickers = [emotion1, emotion2, emotion3, emotion4, emotion5];
    const moods = ['very bad', 'bad', 'not bad', 'good', 'awesome']

    return (
        <>
            <div className={styles.correlationLabel}>
                <h2>Often go together</h2>
            </div>
            <div className={styles.board}>
                <img className={styles.statisticsItemLayout} src={statisticsItem} alt="layout" />
                {moods.map((item, index) =>
                    <div key={index}>
                        {data[item] && data[item].length > 0 ?
                            <div key={index} className={styles.container}>
                                <img src={stickers[index]} alt="sticker"/>
                                {data[item].map((activity, index) =>
                                    <div className={styles.activityLabel} key={index}>{activity}</div>
                                )}
                            </div> :
                            null
                        }
                    </div>
                )}
            </div>
        </>
    );
};

export default StatisticsCorrelation;
