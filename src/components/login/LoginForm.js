import React, {useState} from 'react';
import {NavLink, Redirect} from 'react-router-dom';

import {connect} from "react-redux";
import {userLoginFetch} from "../../redux/authorization/actions";

import styles from '../registration/RegistrationForm.module.css';
import registrationCenter from "../../images/registrationCenter.png";

import Button from "../button/Button";
import Layout from "../layout/Layout";
import classNames from "classnames/bind";

const LoginForm = ({userLoginFetch, user}) => {
    const cx = classNames.bind(styles);
    const [inputValues, setInputValues] = useState({email: '', password: ''});
    const [redirect, setRedirect] = useState(false);
    const [error, setError] = useState('');
    const [userName, setUsername] = useState('')

    const handleSubmit = (event) => {
        event.preventDefault();
        if (inputValues.email && inputValues.password) {
            userLoginFetch(inputValues)
                .then((username) => {
                    setError('');
                    setUsername(username);
                    setRedirect(true);
                })
                .catch(err => {
                    setRedirect(false);
                    setError(err);
                })
        } else {
            setError('Error: Fill in all fields')
        }
    };
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setInputValues({...inputValues, [name]: value});
    };

    if (redirect) {
        return <Redirect to={`/${userName}`}></Redirect>;
    }
    return (
        <>
            <form onSubmit={handleSubmit}>
                <img className={styles.centerImage} src={registrationCenter} alt="centerImage" />
                <h1>Login</h1>
                <div className={styles.inputsBlock}>
                    <input className={cx({errorInput: error})}
                           type="text"
                           value={inputValues.email}
                           name="email"
                           placeholder="Email"
                           onChange={handleInputChange} />
                    <input className={cx({errorInput: error})}
                           type="password" value={inputValues.password}
                           name="password"
                           placeholder="Password"
                           onChange={handleInputChange} />
                    {error ? <h2 className={styles.errorLabel}>{error.toString()}</h2> : null}
                </div>
                <div className={styles.buttonAlone}>
                    <Button id={0}>Login</Button>
                </div>
                <div className={styles.buttonsRow}>
                    <NavLink to={`/`}>
                        <Button id={0}>Back</Button>
                    </NavLink>
                    <NavLink to={`/register`}>
                        <Button id={0}>Register</Button>
                    </NavLink>
                </div>
            </form>
            <Layout pageName="LoginPage"></Layout>
        </>
    );
};

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch => {
    return {
        userLoginFetch: (userInfo) => dispatch(userLoginFetch(userInfo)),
        dispatch,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

