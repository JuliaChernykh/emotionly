import React from 'react';

import styles from './Logo.module.css';
import designElementLogo from "../../images/designElementLogo.png";

const Logo = () => {
    return (
        <div className={styles.logo}>
            <h1>Emotionly</h1>
            <img src={designElementLogo} alt="designElement" />
        </div>
    );
};

export default Logo;
