import React from 'react';
import classNames from 'classnames/bind';

import styles from './Button.module.css';

const Button = ({ type, children, onClick, id }) => {
    const cx = classNames.bind(styles);

    return (
        <button className={cx({
            buttonType1: id === 0,
            buttonType2: id === 1
        })} onClick={onClick} type={type}>{children}</button>
    );
};

export default Button;
