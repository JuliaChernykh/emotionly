import React from 'react';
import classNames from 'classnames/bind';

import styles from './Layout.module.css';
import StartPage_1 from "../../images/StartPage_1.png";
import StartPage_2 from "../../images/StartPage_2.png";
import mainPage_1 from "../../images/mainPage_1.png"
import mainPage_2 from "../../images/mainPage_2.png"
import mainPage_3 from "../../images/navBarDesign.png"

const Layout = ({pageName}) => {
    const test = {'StartPage': 0, 'LoginPage': 0, 'RegistrationPage': 0, 'MainUserPage': 1, 'StatisticsPage': 1,
    'NotFountPage': 0, 'SuccessfulSavingPage': 1, 'DailyActivityPage': 1};
    const layouts = [StartPage_1, StartPage_2, mainPage_1, mainPage_2, mainPage_3];
    const cx = classNames.bind(styles);

    return (
        <>
            <div className={cx({
                StartPage_1: test[pageName] === 0,
                MainPage_1: test[pageName] === 1
            })}>
                <img src={layouts[0 + 2*test[pageName]]} alt={`designElement${test[pageName]}`} />
            </div>
            <div className={cx({
                StartPage_2: test[pageName] === 0,
                MainPage_2: test[pageName] === 1
             })}>
                <img src={layouts[1 + 2*test[pageName]]} alt={`designElement${test[pageName]}`} />
            </div>
            {test[pageName] === 1 ?
                <div className={styles.MainPage_3}>
                    <img src={layouts[2 + 2*test[pageName]]} alt="navBarDesign" />
                </div> :
                null
            }

        </>
    );
};

export default Layout;
