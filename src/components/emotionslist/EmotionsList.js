import React, {useState} from "react";

import classNames from 'classnames/bind';
import styles from "./EmotionsList.module.css";

import emotion1 from "../../images/emotion1.png";
import emotion2 from "../../images/emotion2.png";
import emotion3 from "../../images/emotion3.png";
import emotion4 from "../../images/emotion4.png";
import emotion5 from "../../images/emotion5.png";

const EmotionsList = () => {
    const cx = classNames.bind(styles);
    const stickers = [emotion1, emotion2, emotion3, emotion4, emotion5];
    const moods = ['very bad', 'bad', 'not bad', 'good', 'awesome']
    const [selectedItem, setItem] = useState(moods.indexOf(localStorage.getItem("mood")));

    const handleClick = (event, index) => {
        event.preventDefault();
        setItem(index);
        localStorage.setItem("mood", moods[index]);
    }


    return (
        <div className={styles.container}>
            {stickers.map((sticker, index) =>
                <img className={cx({selectedImg: index === selectedItem})}
                     key={index}
                     src={sticker}
                     alt="emotionImage"
                     onClick={(event) => handleClick(event, index)}
                />
            )}
        </div>

    );
};

export default EmotionsList;
