import React from "react";

import styles from './Loader.module.css'

const Loader = () => {
    return (
        <h1 className={styles.label}>Loading...</h1>
    )
}

export default Loader;
