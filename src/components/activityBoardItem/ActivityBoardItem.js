import React, {useState} from 'react';

import styles from './ActivityBoardItem.module.css';
import classNames from 'classnames/bind';
import Button from "../button/Button";

import * as CgIcons from 'react-icons/cg'
import { IconContext } from 'react-icons';
import activityType from '../../images/activityType.png'
import activityBoardItem from '../../images/activityBoardItem.png'

import {connect} from "react-redux";

import {updateUserActivities} from "../../requests/requests";

const ActivityBoardItem = ({type, options, userActivities}) => {
    const cx = classNames.bind(styles);
    let selectedItemsNames = JSON.parse(localStorage.getItem(type)) || [];
    let selectedItems = [];
    selectedItemsNames.forEach(item => {
        selectedItems.push(options.indexOf(item));
    })

    const [editingMode, setEditingMode] = useState(false)
    const [inputMode, setInputMode] = useState(false)
    const [inputValue, setInputValue] = useState('');
    const [rerender, setRerender] = useState(false);

    const handleClick = (event, index) => {
        event.preventDefault();
        if (!editingMode) {
            if (selectedItems.includes(index)){
                selectedItems = selectedItems.filter(item => item !== index);
                selectedItemsNames = selectedItemsNames.filter(item => item !== options[index]);
            } else {
                selectedItems.push(index);
                selectedItemsNames.push(options[index]);
            }
            localStorage.setItem(type, JSON.stringify(selectedItemsNames))
            setRerender(!rerender);
        }
    }

    const handleEditButtonClick = () => {
        if (editingMode) {
            if (inputValue) {
                options.push(inputValue);
            }
            setInputValue('');
            setInputMode(false);
            // отправляем изменения
            updateUserActivities(userActivities);
        }
        setEditingMode(!editingMode);
    }
    const handleAddButtonClick = () => {
        setInputMode(!inputMode);
    }
    const handleKeyPress = (event) => {
        if(event.key === 'Enter') {
            if (inputValue && !options.includes(inputValue)) {
                options.push(inputValue);
            }
            console.log(options)
            setInputMode(!inputMode);
            setInputValue('');
        }
    }
    const handleInputChange = (event) => {
        setInputValue(event.target.value);
    };

    const handleDeletingClick = (event, index) => {
        event.preventDefault();
        if (editingMode) {
            options.splice(index, 1);
            selectedItems = selectedItems.filter(item => item !== index);
            selectedItemsNames = selectedItemsNames.filter(item => item !== options[index]);
            localStorage.setItem(type, JSON.stringify(selectedItemsNames));
            setRerender(!rerender);
        }
    }

    return (
        <IconContext.Provider value={{color: '#000000', size: 17}}>
            <div className={styles.board}>
                <img className={styles.boardItemLayout} src={activityBoardItem} alt="layout" />
                <div className={styles.activityType}>
                    <img src={activityType} alt="layout" />
                    <p>{type}</p>
                </div>
                <div className={styles.activitiesContainer}>
                    {options.map((item, index) =>
                        <p className={cx({selectedActivity: selectedItems.includes(index) && !editingMode})}
                           key={index}
                           onClick={(event) => handleClick(event, index)}
                        >
                            {item}
                            <button className={cx({editingMode: editingMode})} onClick={(event) => handleDeletingClick(event, index)}>
                            <i key={index}
                               className={cx({editingMode: editingMode})}
                            >
                                <CgIcons.CgClose/>
                            </i>
                            </button>
                        </p>
                    )}
                </div>
                <div className={styles.flexButtons}>
                    {editingMode ?
                        (<div className={cx({disButton: true, editingMode: editingMode, inputMode: inputMode})}>
                        <Button id={1} onClick={handleAddButtonClick}>Add</Button>
                        <input type="text"
                               value={inputValue}
                               onChange={handleInputChange}
                               onKeyPress={handleKeyPress}
                               maxLength="12"
                        >
                        </input>
                        </div>) :
                        null
                    }
                    <Button id={1} onClick={handleEditButtonClick}>{editingMode ? 'Save' : 'Edit/New'}</Button>
                </div>
            </div>
        </IconContext.Provider>
    );
};

const mapStateToProps = state => ({ userActivities: state.activities });

export {ActivityBoardItem};

export default connect(
    mapStateToProps,
   null
)(ActivityBoardItem);
