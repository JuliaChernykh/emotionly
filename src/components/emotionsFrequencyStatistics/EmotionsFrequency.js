import React from "react";

import styles from './EmotionsFrequency.module.css'

import emotion1 from "../../images/emotion1.png";
import emotion2 from "../../images/emotion2.png";
import emotion3 from "../../images/emotion3.png";
import emotion4 from "../../images/emotion4.png";
import emotion5 from "../../images/emotion5.png";
import statisticsItem from "../../images/statisticsItem.png";

const EmotionsFrequency = ({total, data}) => {
    const stickers = [emotion1, emotion2, emotion3, emotion4, emotion5];
    const moods = ['very bad', 'bad', 'not bad', 'good', 'awesome']

    const moodsFrequency = new Map();
    moods.forEach(item => {
        moodsFrequency.set(item, data[item] || '0')
    })

    return (
        <>
            <div className={styles.moodCountLabel}>
                <h2>You've filled your mood for {total} times</h2>
            </div>
            <div className={styles.board}>
                <img className={styles.statisticsItemLayout} src={statisticsItem} alt="layout" />
                <div className={styles.container}>
                    {stickers.map((sticker, index) =>
                        <div key={index} className={styles.individualStatisticsContainer}>
                            <img key={Date.now()}
                                 src={sticker}
                                 alt="emotionImage"
                            />
                            {total !== 0 ?
                                <h2 key={Date.now() + 1}>
                                    {moodsFrequency.get(moods[index]) ?
                                        Math.floor(((moodsFrequency.get(moods[index])/total)*100)).toString()+'%' :
                                        '0%'
                                    }
                                </h2> :
                                null
                            }
                        </div>
                    )}
                </div>
            </div>
        </>
    );
};

export default EmotionsFrequency;
