import {
    LOAD_LIST_FAILURE,
    LOAD_LIST_SUCCESS,
} from './actions';

const initialState = {};

function reducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_LIST_FAILURE:
            return null;
        case LOAD_LIST_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;
