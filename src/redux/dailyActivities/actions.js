import {getActivities} from "../../services/requests";

export const LOAD_LIST_FAILURE = 'LOAD_LIST_FAILURE';
export const LOAD_LIST_SUCCESS = 'LOAD_LIST_SUCCESS';

export const loadListFailure = {
    type: LOAD_LIST_FAILURE,
}

export const loadListSuccess = (userList) => ({
    type: LOAD_LIST_SUCCESS,
    payload: userList
})

export const loadActivities = () => (dispatch) => {
    return getActivities()
        .then(list => {
            dispatch(loadListSuccess(list));
            return list;
        })
        .catch(error => dispatch(loadListFailure))
}


