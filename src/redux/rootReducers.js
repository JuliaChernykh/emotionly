import user from './authorization/reducer';
import activities from './dailyActivities/reducer';
import statistics from './statistics/reducer'

import {combineReducers} from "redux";

const allReducers = combineReducers({
    user,
    activities,
    statistics
});

export default allReducers;
