export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';

export const loginUser = (user) => ({
    type: LOGIN_USER,
    payload: user
})

export const logoutUser = () => ({
    type: LOGOUT_USER
})


export const userPostFetch = (user) => (dispatch) => {
    return fetch("http://localhost:8000/users", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify({user})
    })
        // .then((response) => {
        //     if (!response.ok) {
        //         throw Error(response.statusText);
        //     }
        //     return response;
        // })
        .then(resp => resp.json())
        .then(data => {
            if (data.errors) {
                const errorHeaders = Object.keys(data.errors)
                let errors = {}
                errorHeaders.forEach(key => {
                    errors[key] = data.errors[key][0];
                })
                throw JSON.stringify(errors);
            }
            localStorage.setItem("token", data.user.token);
            dispatch(loginUser(data.user));
            return data.user.username;
        })
}

export const userLoginFetch = user => (dispatch) => {
    return fetch("http://localhost:8000/login", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify({user})
    })
        // .then((response) => {
        //     console.log(response.body)
        //     if (!response.ok) {
        //         throw Error(response.statusText);
        //     }
        //     return response;
        // })
        .then(resp => resp.json())
        .then(data => {
            if (data.errors) {
                throw Error(data.errors.error[0]);
            }
            localStorage.setItem("token", data.user.token)
            dispatch(loginUser(data.user))
            return data.user.username;
        })
}

export const getProfileFetch = user => (dispatch) => {
    const token = localStorage.getItem("token");
    if (token) {
        return fetch("http://localhost:8000/profile", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then(resp => resp.json())
            .then(data => {
                if (data.message) {
                    localStorage.removeItem("token")
                } else {
                    dispatch(loginUser(data.user))
                }
            })
    }
}
