import {getStatistics} from "../../requests/requests";

export const LOAD_STATISTICS_FAILURE = 'LOAD_STATISTICS_FAILURE';
export const LOAD_STATISTICS_SUCCESS = 'LOAD_STATISTICS_SUCCESS';

export const loadStatisticsFailure = {
    type: LOAD_STATISTICS_FAILURE,
}

export const loadStatisticsSuccess = (statistics) => ({
    type: LOAD_STATISTICS_SUCCESS,
    payload: statistics
})

export const loadStatistics = () => (dispatch) => {
    return getStatistics()
        .then(statistics => {
            dispatch(loadStatisticsSuccess(statistics));
            return statistics;
        })
        .catch(error => dispatch(loadStatisticsFailure))
}
