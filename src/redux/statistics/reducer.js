import {
    LOAD_STATISTICS_FAILURE,
    LOAD_STATISTICS_SUCCESS
} from "./actions";

const initialState = {};

function reducer(state = initialState, action) {
    switch (action.type) {
        case LOAD_STATISTICS_FAILURE:
            return null;
        case LOAD_STATISTICS_SUCCESS:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;
