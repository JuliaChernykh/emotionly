import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Logo from "./components/logo/Logo";
import StartPage from "./pages/StartPage";
import RegistrationForm from "./components/registration/RegistrationForm";
import LoginForm from "./components/login/LoginForm";
import MainUserPage from "./pages/MainUserPage";
import DailyActivityPage from "./pages/DailyActivityPage";
import StatisticsPage from "./pages/StatisticsPage";
import NotFoundPage from "./pages/NotFoundPage";
import SuccessfulSavingPage from "./pages/SuccessfulSavingPage";
import ProtectedRoute from "./components/protectedRoute/ProtectedRoute";

function App() {

    const renderRegistrationFrom = () => (
        <RegistrationForm/>
    );
    const renderLoginFrom = () => (
        <LoginForm/>
    );
    const renderMainUserPage = () => (
        <MainUserPage/>
    );
    const renderStartPage = () => (
        <StartPage/>
    )

  return (
      <Router>
          <Logo></Logo>
          <Switch>
              <Route
                  exact path="/register"
                  render={renderRegistrationFrom}
              />
              <Route
                  exact path="/login"
                  render={renderLoginFrom}
              />
              <Route
                  exact path="/main"
                  render={renderMainUserPage}
              />
              <ProtectedRoute
                  exact path="/:username"
                  component={MainUserPage}
              />
              <ProtectedRoute
                  exact path="/:username/activity"
                  component={DailyActivityPage}
              />
              <ProtectedRoute
                  exact path="/:username/statistics"
                  component={StatisticsPage}
              />
              <ProtectedRoute
                  exact path="/:username/saved"
                  component={SuccessfulSavingPage}
              />
              <Route
                  exact path="/"
                  render={renderStartPage}
              />
              <Route component={NotFoundPage} />
          </Switch>

      </Router>
  );
}

// const mapStateToProps = state => ({ user: state.user });
// const mapDispatchToProps = dispatch => ({
//     getProfileFetch: () => dispatch(getProfileFetch())
// })

// export default connect(mapStateToProps, null)(App);

export default App;
